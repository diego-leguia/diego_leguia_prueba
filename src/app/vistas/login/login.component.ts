import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiLoginService } from '../../servicios/api/api-login.service';
import { LoginI } from '../../modelos/login_interface';
import { ResponseI } from '../../modelos/response.interface';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    usuario: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  })

  constructor(private api: ApiLoginService, private router: Router) { }

  errorStatus: boolean = false;
  errorMsj = "";

  ngOnInit(): void {
  }

  onLogin(form: LoginI) {
    this.api.loginByEmail(form).subscribe(data => {

      let dataResponse: ResponseI = data;
      if (dataResponse.status == "ok") {
        localStorage.setItem('token', dataResponse.result.token);
        this.router.navigate(['dashboard']);
      }else{
        this.errorStatus = true;
        this.errorMsj = dataResponse.result.error_msg;
      }
    });
  }

}
