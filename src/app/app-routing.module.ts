import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './vistas/dashboard/dashboard.component';
import { LoginComponent } from './vistas/login/login.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '',
    redirectTo: 'login',
    pathMatch:'full'},
  { path:'login',
    component : LoginComponent
  },
  { path:'dashboard',
    component : DashboardComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent,DashboardComponent]