import { TestBed } from '@angular/core/testing';

import { ApiLOGINService } from './api-login.service';

describe('ApiLOGINService', () => {
  let service: ApiLOGINService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiLOGINService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
